![](https://img.shields.io/badge/Microverse-blueviolet)

# SPACE TRAVELERS' HUB

>Space Travelers' Hub is a web application for a company that provides commercial and scientific space travel services. The application will allow users to book rockets and join selected space missions. Application data was fetched from the SpaceX API.

## ADDITIONAL DESCRIPTION ABOUT THE APPLICATION

The Space Travelers' Hub consists of Rockets, Missions, and the My Profile section.

### Rockets 
The Rockets section displays a list of current rockets available for booking.

![Screenshot 2022-01-29 at 11 55 40](https://user-images.githubusercontent.com/31847346/151658353-b639dbf3-aef6-464b-9b5e-abd1174c8d1d.png)

### Missions

The Missions section displays a list of current missions along with their brief description and participation status. There is also a button next to each mission that allows users to join the selected mission or leave the mission the user joined earlier.

![Screenshot 2022-01-29 at 11 55 19](https://user-images.githubusercontent.com/31847346/151658345-42bbfea3-e692-4b30-8d28-4148fc29772a.png)


### My Profile

The My Profile section displays all reserved rockets and space missions.

![Screenshot 2022-01-29 at 11 56 33](https://user-images.githubusercontent.com/31847346/151658372-ad719bbe-2cd0-46b4-8978-fa718dd5c2b0.png)

## BUILT WITH

-- Basic Language - JavaScript 
- Framework - React
- Other technologies/tools: 

``` create-react-app
    > create-react-app
    > webpack for bundling files
    > Babel for code transpiling
    > Git for version control
    > Eslint for JavaScript linting
    > Stylelint for style linting
    > Jest for testing
    > Space-X API
 ```

## Live Demo

[Live Demo Link]( https://kingsleyibe.github.io/ben-kings-react-project/)

## Getting Started

To get a local copy up and running follow these simple example steps.

1. Clone the repository using

```
git clone https://github.com/KingsleyIbe/ben-kings-react-project.git
```

2. cd into the cloned repository

```
cd ben-kings-react-project
```

3. You now have the access to the files on your local machine!

#### Optional steps

Install the node_modules/ folder to be able to locally run the linter commands. Run:

```
npm install
```


To check Stylelint linter errors run:

```
npx stylelint "**/*.scss"
```

To check ESLint errors run:

```
npx eslint .
```

## Authors

👤 **Author1**

- GitHub: [@benjp009](https://github.com/benjp009)
- Twitter: [@benjp009](https://twitter.com/benjp009)
- LinkedIn: [benjaminpatin](https://www.linkedin.com/in/benjaminpatin/)

👤 **Author2**

- GitHub: [@Kingsleyibe](https://github.com/kingsleyibe)
- Twitter: [@ibekingsley2](https://twitter.com/ibekingsley2)
- LinkedIn: [Kingsley Ibe](https://www.linkedin.com/in/kingsley-ibe-5669a5134/)

## 🤝 Contributing

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](https://github.com/KingsleyIbe/ben-kings-react-project/issues).

## Show your support

Give a ⭐️ if you like this project!

## 📝 License

This project is [MIT](./MIT.md) licensed.
